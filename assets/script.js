"use strict";
!function() {
    var t = navigator.userAgent.toLowerCase()
      , M = new Date
      , I = ($(document),
    $(window))
      , E = $("html")
      , T = ($("body"),
    E.hasClass("desktop"))
      , L = -1 !== t.indexOf("msie") ? parseInt(t.split("msie")[1], 10) : -1 !== t.indexOf("trident") ? 11 : -1 !== t.indexOf("edge") && 12
      , i = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
      , P = !1
      , N = {
        bootstrapModal: $(".modal "),
        captcha: $(".recaptcha "),
        campaignMonitor: $(".campaign-mailform "),
        copyrightYear: $(".copyright-year "),
        materialParallax: $(".parallax-container "),
        mailchimp: $(".mailchimp-mailform "),
        owl: $(".owl-carousel "),
        rdNavbar: $(".rd-navbar "),
        rdMailForm: $(".rd-mailform "),
        rdInputLabel: $(".form-label "),
        regula: $("[data-constraints] "),
        search: $(".rd-search "),
        wow: $(".wow "),
        parallaxJs: $(".parallax-scene"),
        swiper: document.querySelectorAll(".swiper-container "),
        counter: document.querySelectorAll(".counter "),
        multiToggles: document.querySelectorAll("[data-multitoggle] "),
        vide: $(".vide "),
        buttonNuka: $(".button-nuka ")
    };
    if (N.rdNavbar.length) {
        var a = N.rdNavbar
          , e = {
            "-": 0,
            "-sm-": 576,
            "-md-": 768,
            "-lg-": 992,
            "-xl-": 1200,
            "-xxl-": 1600
        }
          , r = {};
        for (var n in e) {
            var o = r[e[n]] = {};
            a.attr("data" + n + "layout") && (o.layout = a.attr("data" + n + "layout")),
            a.attr("data" + n + "device-layout") && (o.deviceLayout = a.attr("data" + n + "device-layout")),
            a.attr("data" + n + "hover-on") && (o.focusOnHover = "true" === a.attr("data" + n + "hover-on")),
            a.attr("data" + n + "auto-height") && (o.autoHeight = "true" === a.attr("data" + n + "auto-height")),
            a.attr("data" + n + "stick-up-offset") && (o.stickUpOffset = a.attr("data" + n + "stick-up-offset")),
            a.attr("data" + n + "stick-up") && (o.stickUp = "true" === a.attr("data" + n + "stick-up")),
            P ? o.stickUp = !1 : a.attr("data" + n + "stick-up") && (o.stickUp = "true" === a.attr("data" + n + "stick-up"))
        }
        N.rdNavbar.RDNavbar({
            anchorNav: !P,
            stickUpClone: !(!N.rdNavbar.attr("data-stick-up-clone") || P) && "true" === N.rdNavbar.attr("data-stick-up-clone"),
            responsive: r,
            callbacks: {
                onStuck: function() {
                    var t = this.$element.find(".rd-search input");
                    t && t.val("").trigger("propertychange")
                },
                onDropdownOver: function() {
                    return !P
                },
                onUnstuck: function() {
                    var t;
                    null === this.$clone || (t = this.$clone.find(".rd-search input")) && (t.val("").trigger("propertychange"),
                    t.trigger("blur"))
                }
            }
        })
    }
    console.log(""),
    I.on("load", function() {
        if (N.counter)
            for (r = 0; r < N.counter.length; r++) {
                var a = N.counter[r]
                  , t = (aCounter({
                    node: a,
                    duration: a.getAttribute("data-duration") || 1e3,
                    formatter: a.hasAttribute("data-formatter") ? function(t) {
                        return Number(t.toFixed()).toLocaleString(a.getAttribute("data-formatter"))
                    }
                    : null
                }),
                function() {
                    Util.inViewport(this) && !this.classList.contains("animated-first") && (this.counter.run(),
                    this.classList.add("animated-first"))
                }
                .bind(a))
                  , e = function() {
                    this.counter.params.to = parseInt(this.textContent, 10),
                    this.counter.run()
                }
                .bind(a);
                P ? (a.counter.run(),
                a.addEventListener("blur", e)) : (t(),
                window.addEventListener("scroll", t))
            }
        if (N.materialParallax.length)
            if (P || L || i)
                for (var r = 0; r < N.materialParallax.length; r++) {
                    (a = $(N.materialParallax[r])).addClass("parallax-disabled"),
                    a.css({
                        "background-image": "url(" + a.data("parallax-img") + ")"
                    })
                }
            else
                N.materialParallax.parallax()
    }),
    document.addEventListener("StartOtherLoading", function(t) {
        function a(t) {
            for (var a = 0; a < t.length; a++) {
                for (var e = t[a].querySelectorAll('[aria-hidden="true"]'), r = t[a].querySelectorAll('[aria-hidden="false"]'), i = 0; i < e.length; i++)
                    e[i].setAttribute("aria-hidden", "false");
                for (i = 0; i < r.length; i++)
                    r[i].setAttribute("aria-hidden", "true")
            }
        }
        function i(t) {
            var a = t.$wrapperEl[0].children[t.activeIndex];
            t.realPrevious = Array.prototype.indexOf.call(a.parentNode.children, a)
        }
        function n(t) {
            var a = t.el.querySelectorAll("[data-slide-bg]");
            for (t = 0; t < a.length; t++) {
                var e = a[t];
                e.style.backgroundImage = "url(" + e.getAttribute("data-slide-bg") + ")"
            }
        }
        function e(t) {
            for (var a = ["-", "-sm-", "-md-", "-lg-", "-xl-", "-xxl-"], e = [0, 576, 768, 992, 1200, 1600], r = {}, i = 0; i < e.length; i++) {
                r[e[i]] = {};
                for (var n = i; -1 <= n; n--)
                    !r[e[i]].items && t.attr("data" + a[n] + "items") && (r[e[i]].items = n < 0 ? 1 : parseInt(t.attr("data" + a[n] + "items"), 10)),
                    !r[e[i]].stagePadding && 0 !== r[e[i]].stagePadding && t.attr("data" + a[n] + "stage-padding") && (r[e[i]].stagePadding = n < 0 ? 0 : parseInt(t.attr("data" + a[n] + "stage-padding"), 10)),
                    !r[e[i]].margin && 0 !== r[e[i]].margin && t.attr("data" + a[n] + "margin") && (r[e[i]].margin = n < 0 ? 30 : parseInt(t.attr("data" + a[n] + "margin"), 10))
            }
            t.on("initialized.owl.carousel", function() {
                o(t.find('[data-lightgallery="item"]'), "lightGallery-in-carousel")
            }),
            t.owlCarousel({
                autoplay: !P && "false" !== t.attr("data-autoplay"),
                autoplayTimeout: t.attr("data-autoplay-time-out") ? Number(t.attr("data-autoplay-time-out")) : 3e3,
                smartSpeed: t.attr("data-smart-speed") ? Number(t.attr("data-smart-speed")) : 250,
                autoplayHoverPause: "true" === t.attr("data-autoplay-hover-pause"),
                URLhashListener: "true" === t.attr("data-hash-navigation") || !1,
                startPosition: "URLHash",
                slideTransition: t.attr("data-slide-transition") ? t.attr("data-slide-transition") : "",
                loop: !P && "false" !== t.attr("data-loop"),
                items: 1,
                autoHeight: 1,
                center: "true" === t.attr("data-center"),
                dotsContainer: t.attr("data-pagination-class") || !1,
                navContainer: t.attr("data-navigation-class") || !1,
                mouseDrag: !P && "false" !== t.attr("data-mouse-drag"),
                nav: "true" === t.attr("data-nav"),
                dots: "true" === t.attr("data-dots"),
                dotsEach: !!t.attr("data-dots-each") && parseInt(t.attr("data-dots-each"), 10),
                animateIn: !!t.attr("data-animation-in") && t.attr("data-animation-in"),
                animateOut: !!t.attr("data-animation-out") && t.attr("data-animation-out"),
                responsive: r,
                navText: t.attr("data-nav-text") ? $.parseJSON(t.attr("data-nav-text")) : [],
                navClass: t.attr("data-nav-class") ? $.parseJSON(t.attr("data-nav-class")) : ["owl-prev", "owl-next"]
            })
        }
        function d(t, a) {
            var e, r, i = 0;
            if (t.length) {
                for (var n = 0; n < t.length; n++) {
                    var o = $(t[n]);
                    if ((e = o.regula("validate")).length)
                        for (f = 0; f < e.length; f++)
                            i++,
                            o.siblings(".form-validation").text(e[f].message).parent().addClass("has-error");
                    else
                        o.siblings(".form-validation").text("").parent().removeClass("has-error")
                }
                return a && a.length ? 0 !== (r = a).find(".g-recaptcha-response").val().length ? 0 === i : (r.siblings(".form-validation").html("Please, prove that you are not robot.").addClass("active"),
                r.closest(".form-wrap").addClass("has-error"),
                r.on("propertychange", function() {
                    var t = $(this);
                    0 < t.find(".g-recaptcha-response").val().length && (t.closest(".form-wrap").removeClass("has-error"),
                    t.siblings(".form-validation").removeClass("active").html(""),
                    t.off("propertychange"))
                }),
                0) : 0 === i
            }
            return 1
        }
        function r(t, a) {
            P || $(t).on("click", function() {
                $(t).lightGallery({
                    thumbnail: "false" !== $(t).attr("data-lg-thumbnail"),
                    selector: "[data-lightgallery='item']",
                    autoplay: "true" === $(t).attr("data-lg-autoplay"),
                    pause: parseInt($(t).attr("data-lg-autoplay-delay")) || 5e3,
                    addClass: a,
                    mode: $(t).attr("data-lg-animation") || "lg-slide",
                    loop: "false" !== $(t).attr("data-lg-loop"),
                    dynamic: !0,
                    dynamicEl: JSON.parse($(t).attr("data-lg-dynamic-elements")) || []
                })
            })
        }
        function o(t, a) {
					  return false;
            P || $(t).lightGallery({
                selector: "this",
                addClass: a,
                counter: !1,
                youtubePlayerParams: {
                    modestbranding: 1,
                    showinfo: 0,
                    rel: 0,
                    controls: 0
                },
                vimeoPlayerParams: {
                    byline: 0,
                    portrait: 0
                }
            })
        }
        if (console.log("script.min.js - dev1.01"),
        P = window.xMode,
        window.onloadCaptchaCallback = function() {
            for (var t = 0; t < N.captcha.length; t++) {
                var a = $(N.captcha[t])
                  , e = function() {
                    var t, a, e, r = this.querySelector("iframe"), i = this.firstElementChild, n = i.firstElementChild;
                    n.style.transform = "",
                    i.style.height = "auto",
                    i.style.width = "auto",
                    t = this.getBoundingClientRect(),
                    a = r.getBoundingClientRect(),
                    (e = t.width / a.width) < 1 && (n.style.transform = "scale(" + e + ")",
                    i.style.height = a.height * e + "px",
                    i.style.width = a.width * e + "px")
                }
                .bind(N.captcha[t]);
                grecaptcha.render(a.attr("id"), {
                    sitekey: a.attr("data-sitekey"),
                    size: a.attr("data-size") ? a.attr("data-size") : "normal",
                    theme: a.attr("data-theme") ? a.attr("data-theme") : "light",
                    callback: function() {
                        $(".recaptcha").trigger("propertychange")
                    }
                }),
                a.after("<span class='form-validation'></span>"),
                N.captcha[t].hasAttribute("data-auto-size") && (e(),
                window.addEventListener("resize", e))
            }
        }
        ,
        N.captcha.length && $.getScript("//www.google.com/recaptcha/api.js?onload=onloadCaptchaCallback&render=explicit&hl=en"),
        navigator.platform.match(/(Mac)/i) && E.addClass("mac-os"),
        L && (12 === L && E.addClass("ie-edge"),
        11 === L && E.addClass("ie-11"),
        L < 10 && E.addClass("lt-ie-10"),
        L < 11 && E.addClass("ie-10")),
        N.bootstrapModal.length)
            for (var l = 0; l < N.bootstrapModal.length; l++) {
                var s = $(N.bootstrapModal[l]);
                s.on("hidden.bs.modal", $.proxy(function() {
                    var t, a = $(this), e = a.find("video"), r = a.find("iframe");
                    e.length && e[0].pause(),
                    r.length && (t = r.attr("src"),
                    r.attr("src", "").attr("src", t))
                }, s))
            }
        if (N.copyrightYear.length && N.copyrightYear.text(M.getFullYear()),
        T && !P && $().UItoTop({
            easingType: "easeOutQuad",
            containerClass: "ui-to-top fa fa-angle-up"
        }),
        N.swiper.length)
            for (l = 0; l < N.swiper.length; l++) {
                var c = N.swiper[l]
                  , u = {
                    loop: "true" === c.getAttribute("data-loop") || !1,
                    effect: !L && c.getAttribute("data-effect") || "slide",
                    direction: c.getAttribute("data-direction") || "horizontal",
                    speed: c.getAttribute("data-speed") ? Number(c.getAttribute("data-speed")) : 1e3,
                    allowTouchMove: !1,
                    preventIntercationOnTransition: !0,
                    runCallbacksOnInit: !1,
                    separateCaptions: "true" === c.getAttribute("data-separate-captions") || !1
                };
                c.getAttribute("data-autoplay") && (u.autoplay = {
                    delay: Number(c.getAttribute("data-autoplay")) || 3e3,
                    stopOnLastSlide: !1,
                    disableOnInteraction: !0,
                    reverseDirection: !1
                }),
                "true" === c.getAttribute("data-keyboard") && (u.keyboard = {
                    enabled: "true" === c.getAttribute("data-keyboard"),
                    onlyInViewport: !0
                }),
                "true" === c.getAttribute("data-mousewheel") && (u.mousewheel = {
                    releaseOnEdges: !0,
                    sensitivity: .1
                }),
                c.querySelector(".swiper-button-next, .swiper-button-prev") && (u.navigation = {
                    nextEl: ".swiper-button-next",
                    prevEl: ".swiper-button-prev"
                }),
                c.querySelector(".swiper-pagination") && (u.pagination = {
                    el: ".swiper-pagination",
                    type: "bullets",
                    clickable: !0
                }),
                c.querySelector(".swiper-scrollbar") && (u.scrollbar = {
                    el: ".swiper-scrollbar",
                    hide: !0,
                    draggable: !0
                }),
                u.on = {
                    init: function() {
                        var t, r, a, e;
                        n(this),
                        i(this),
                        r = function(a) {
                            return function() {
                                var t;
                                (t = a.getAttribute("data-caption-duration")) && (a.style.animationDuration = t + "ms"),
                                a.classList.remove("not-animated"),
                                a.classList.add(a.getAttribute("data-caption-animate")),
                                a.classList.add("animated")
                            }
                        }
                        ,
                        a = function(t) {
                            for (var a = 0; a < t.length; a++) {
                                var e = t[a];
                                e.classList.remove("animated"),
                                e.classList.remove(e.getAttribute("data-caption-animate")),
                                e.classList.add("not-animated")
                            }
                        }
                        ,
                        e = function(t) {
                            for (var a = 0; a < t.length; a++) {
                                var e = t[a];
                                e.getAttribute("data-caption-delay") ? setTimeout(r(e), Number(e.getAttribute("data-caption-delay"))) : r(e)()
                            }
                        }
                        ,
                        (t = this).params.caption = {
                            animationEvent: "slideChangeTransitionEnd"
                        },
                        a(t.$wrapperEl[0].querySelectorAll("[data-caption-animate]")),
                        e(t.$wrapperEl[0].children[t.activeIndex].querySelectorAll("[data-caption-animate]")),
                        "slideChangeTransitionEnd" === t.params.caption.animationEvent ? t.on(t.params.caption.animationEvent, function() {
                            a(t.$wrapperEl[0].children[t.previousIndex].querySelectorAll("[data-caption-animate]")),
                            e(t.$wrapperEl[0].children[t.activeIndex].querySelectorAll("[data-caption-animate]"))
                        }) : (t.on("slideChangeTransitionEnd", function() {
                            a(t.$wrapperEl[0].children[t.previousIndex].querySelectorAll("[data-caption-animate]"))
                        }),
                        t.on(t.params.caption.animationEvent, function() {
                            e(t.$wrapperEl[0].children[t.activeIndex].querySelectorAll("[data-caption-animate]"))
                        })),
                        this.on("slideChangeTransitionEnd", function() {
                            i(this)
                        })
                    }
                },
                new Swiper(N.swiper[l],u)
            }
        if (N.owl.length)
            for (l = 0; l < N.owl.length; l++) {
                var p = $(N.owl[l]);
                e(N.owl[l].owl = p)
            }
        if (E.hasClass("wow-animation") && N.wow.length && !P && T && (new WOW).init(),
        N.rdInputLabel.length && N.rdInputLabel.RDInputLabel(),
        N.regula.length && function(t) {
            regula.custom({
                name: "PhoneNumber",
                defaultMessage: "Invalid phone number format",
                validator: function() {
                    return "" === this.value || /^(\+\d)?[0-9\-\(\) ]{5,}$/i.test(this.value)
                }
            });
            for (var r = 0; r < t.length; r++) {
                var a = $(t[r]);
                a.addClass("form-control-has-validation").after("<span class='form-validation'></span>"),
                a.parent().find(".form-validation").is(":last-child") && a.addClass("form-control-last-child")
            }
            t.on("input change propertychange blur", function(t) {
                var a, e = $(this);
                if (("blur" === t.type || e.parent().hasClass("has-error")) && !e.parents(".rd-mailform").hasClass("success"))
                    if ((a = e.regula("validate")).length)
                        for (r = 0; r < a.length; r++)
                            e.siblings(".form-validation").text(a[r].message).parent().addClass("has-error");
                    else
                        e.siblings(".form-validation").text("").parent().removeClass("has-error")
            }).regula("bind");
            for (var e = [{
                type: regula.Constraint.Required,
                newMessage: "The text field is required."
            }, {
                type: regula.Constraint.Email,
                newMessage: "The email is not a valid email."
            }, {
                type: regula.Constraint.Numeric,
                newMessage: "Only numbers are required"
            }, {
                type: regula.Constraint.Selected,
                newMessage: "Please choose an option."
            }], r = 0; r < e.length; r++) {
                var i = e[r];
                regula.override({
                    constraintType: i.type,
                    defaultMessage: i.newMessage
                })
            }
        }(N.regula),
        N.mailchimp.length)
            for (l = 0; l < N.mailchimp.length; l++) {
                var g = $(N.mailchimp[l])
                  , m = g.find('input[type="email"]');
                g.attr("novalidate", "true"),
                m.attr("name", "EMAIL"),
                g.on("submit", $.proxy(function(e, t) {
                    t.preventDefault();
                    var o = this
                      , a = {}
                      , r = o.attr("action").replace("/post?", "/post-json?").concat("&c=?")
                      , i = o.serializeArray()
                      , n = $("#" + o.attr("data-form-output"));
                    for (l = 0; l < i.length; l++)
                        a[i[l].name] = i[l].value;
                    return $.ajax({
                        data: a,
                        url: r,
                        dataType: "jsonp",
                        error: function(t, a) {
                            n.html("Server error: " + a),
                            setTimeout(function() {
                                n.removeClass("active")
                            }, 4e3)
                        },
                        success: function(t) {
                            n.html(t.msg).addClass("active"),
                            e[0].value = "";
                            var a = $('[for="' + e.attr("id") + '"]');
                            a.length && a.removeClass("focus not-empty"),
                            setTimeout(function() {
                                n.removeClass("active")
                            }, 6e3)
                        },
                        beforeSend: function(t) {
                            var a = window.xMode
                              , e = function() {
                                var t, a = 0, e = o.find("[data-constraints]");
                                if (e.length) {
                                    for (var r = 0; r < e.length; r++) {
                                        var i = $(e[r]);
                                        if ((t = i.regula("validate")).length)
                                            for (var n = 0; n < t.length; n++)
                                                a++,
                                                i.siblings(".form-validation").text(t[n].message).parent().addClass("has-error");
                                        else
                                            i.siblings(".form-validation").text("").parent().removeClass("has-error")
                                    }
                                    return 0 === a
                                }
                                return !0
                            }();
                            if (a || !e)
                                return !1;
                            n.html("Submitting...").addClass("active")
                        }
                    }),
                    !1
                }, g, m))
            }
        if (N.campaignMonitor.length)
            for (l = 0; l < N.campaignMonitor.length; l++) {
                var h = $(N.campaignMonitor[l]);
                h.on("submit", $.proxy(function(t) {
                    for (var a = {}, e = this.attr("action"), r = this.serializeArray(), i = $("#" + N.campaignMonitor.attr("data-form-output")), n = $(this), o = 0; o < r.length; o++)
                        a[r[o].name] = r[o].value;
                    $.ajax({
                        data: a,
                        url: e,
                        dataType: "jsonp",
                        error: function(t, a) {
                            i.html("Server error: " + a),
                            setTimeout(function() {
                                i.removeClass("active")
                            }, 4e3)
                        },
                        success: function(t) {
                            i.html(t.Message).addClass("active"),
                            setTimeout(function() {
                                i.removeClass("active")
                            }, 6e3)
                        },
                        beforeSend: function(t) {
                            if (P || !d(n.find("[data-constraints]")))
                                return !1;
                            i.html("Submitting...").addClass("active")
                        }
                    });
                    for (var l = n[0].getElementsByTagName("input"), o = 0; o < l.length; o++) {
                        l[o].value = "";
                        var s = document.querySelector('[for="' + l[o].getAttribute("id") + '"]');
                        s && s.classList.remove("focus", "not-empty")
                    }
                    return !1
                }, h))
            }
        if (N.rdMailForm.length)
            for (var f, v = {
                MF000: "Successfully sent!",
                MF001: "Recipients are not set!",
                MF002: "Form will not work locally!",
                MF003: "Please, define email field in your form!",
                MF004: "Please, define type of your form!",
                MF254: "Something went wrong with PHPMailer!",
                MF255: "Aw, snap! Something went wrong."
            }, l = 0; l < N.rdMailForm.length; l++) {
                var y = $(N.rdMailForm[l]);
                y.attr("novalidate", "novalidate").ajaxForm({
                    data: {
                        "form-type": y.attr("data-form-type") || "contact",
                        counter: l
                    },
                    beforeSubmit: function(t, a, e) {
                        if (!P) {
                            var r = $(N.rdMailForm[this.extraData.counter])
                              , i = r.find("[data-constraints]")
                              , n = $("#" + r.attr("data-form-output"))
                              , o = r.find(".recaptcha");
                            return n.removeClass("active error success"),
                            !!d(i, o) && (r.addClass("form-in-process"),
                            n.hasClass("snackbars") && (n.html('<p><span class="icon text-middle fa fa-circle-o-notch fa-spin icon-xxs"></span><span>Sending</span></p>'),
                            n.addClass("active")),
                            r.addClass("success").removeClass("form-in-process"),
                            setTimeout(function() {
                                n.removeClass("active error success"),
                                r.removeClass("success")
                            }, 3500),
                            console.log("validate ok"),
                            !0)
                        }
                    },
                    error: function(t) {
                        var a, e;
                        P || (a = $("#" + $(N.rdMailForm[this.extraData.counter]).attr("data-form-output")),
                        e = $(N.rdMailForm[this.extraData.counter]),
                        a.text(v[t]),
                        e.removeClass("form-in-process"))
                    },
                    success: function(t) {
                        var a, e, r;
                        P || (a = $(N.rdMailForm[this.extraData.counter]),
                        e = $("#" + a.attr("data-form-output")),
                        r = a.find("select"),
                        a.addClass("success").removeClass("form-in-process"),
                        t = 5 === t.length ? t : "MF255",
                        e.text(v[t]),
                        "MF000" === t ? e.hasClass("snackbars") ? e.html('<p><span class="icon text-middle mdi mdi-check icon-xxs"></span><span>' + v[t] + "</span></p>") : e.addClass("active success") : e.hasClass("snackbars") ? e.html(' <p class="snackbars-left"><span class="icon icon-xxs mdi mdi-alert-outline text-middle"></span><span>' + v[t] + "</span></p>") : e.addClass("active error"),
                        a.clearForm(),
                        r.length && r.select2("val", ""),
                        a.find("input, textarea").trigger("blur"),
                        setTimeout(function() {
                            e.removeClass("active error success"),
                            a.removeClass("success")
                        }, 3500))
                    }
                })
            }
        if (N.lightGallery = $('[data-lightgallery="group"]'),
        N.lightGallery.length)
            for (l = 0; l < N.lightGallery.length; l++)
                b = N.lightGallery[l],
                P || $(b).lightGallery({
                    thumbnail: "false" !== $(b).attr("data-lg-thumbnail"),
                    selector: "[data-lightgallery='item']",
                    autoplay: "true" === $(b).attr("data-lg-autoplay"),
                    pause: parseInt($(b).attr("data-lg-autoplay-delay")) || 5e3,
                    addClass: void 0,
                    mode: $(b).attr("data-lg-animation") || "lg-slide",
                    loop: "false" !== $(b).attr("data-lg-loop")
                });
        var b;
        if (N.lightGalleryItem = $('[data-lightgallery="item"]'),
        N.lightGalleryItem.length)
            for (l = 0; l < N.lightGalleryItem.length; l++)
                o(N.lightGalleryItem[l]);
        if (N.lightDynamicGalleryItem = $('[data-lightgallery="dynamic"]'),
        N.lightDynamicGalleryItem.length)
            for (l = 0; l < N.lightDynamicGalleryItem.length; l++)
                r(N.lightDynamicGalleryItem[l]);
        if (N.multiToggles.length)
            for (l = 0; l < N.multiToggles.length; l++) {
                var w = N.multiToggles[l];
                new Toggle({
                    node: w,
                    targets: document.querySelectorAll(w.getAttribute("data-multitoggle")),
                    scope: document.querySelectorAll(w.getAttribute("data-scope")),
                    isolate: document.querySelectorAll(w.getAttribute("data-isolate"))
                }),
                N.multiToggles[l].classList.contains("content-toggle") && w.addEventListener("click", function() {
                    a(this.mt.targets)
                })
            }
        if (N.parallaxJs.length)
            for (l = 0; l < N.parallaxJs.length; l++) {
                var C = N.parallaxJs[l];
                new Parallax(C)
            }
        if (N.vide.length)
            for (l = 0; l < N.vide.length; l++) {
                var x = $(N.vide[l])
                  , A = (u = x.data("vide-options"),
                x.data("vide-bg"));
                x.vide(A, u);
                var k = x.data("vide").getVideoObject()
                  , S = function(t) {
                    var a = t;
                    P || a.offset().top + a.outerHeight() >= I.scrollTop() && a.offset().top <= I.scrollTop() + I.height() ? this.play() : this.pause()
                }
                .bind(k, x);
                S(),
                P ? k.pause() : document.addEventListener("scroll", S)
            }
        N.buttonNuka.length && N.buttonNuka.on("mouseenter", function(t) {
            var a = $(this).offset()
              , e = t.pageX - a.left
              , r = t.pageY - a.top;
            $(this).find(".button-overlay").css({
                top: r,
                left: e
            })
        }).on("mouseout", function(t) {
            var a = $(this).offset()
              , e = t.pageX - a.left
              , r = t.pageY - a.top;
            $(this).find(".button-overlay").css({
                top: r,
                left: e
            })
        })
    })
}();
